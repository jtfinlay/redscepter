if (typeof define !== 'function') {
	var define = require('../common/amdefine')(module);
}

/**************************************************
** VARIABLES
**************************************************/

define(function() {
	
	var FPS = 60;
	
	
	// Get the angle between the mouse and the localplayer. In radians
	var getRelAngle = function(x1, y1, x2, y2) {
		
		// direction
		var deltaX = x2 - x1;
		var deltaY = y2 - y1;
		
		var angle = Math.atan(deltaY / deltaX);
		
		if (deltaX < 0) angle += Math.PI;
		
		return angle;
	}
	
	// Allows object to be extended
	var Extend = function(source, destination) {

		for (var property in source) 
			if (!destination[property]) // this check allows override
				destination[property] = source[property];
		return destination;
	}
	
	return ({
	
		/** UPDATE / DRAW / ETC **/
		
		UPDATE_DRAW: Math.round(1000/FPS),
		UPDATE_SERVER: Math.round(1000/40), // 40 updates per second
		UPDATE_REQUEST: Math.round(1000/FPS), // works for now
		TIME_DELAY: 80,	// delay between server-state and client-state
		UPDATE_STORAGE_PERIOD: 2000, // time server stores game states
		
		
		/** ENVIRONMENT PHYSICS **/
		
		GRAVITY: 1.3, // gravity effect on players & objects
		TERMINAL_VELOCITY: 26, // max y velocity of object

		
		/** PLAYER VARIABLES **/
		
		JUMP_VELOCITY: -14, // jump velocity of player
		PLAYER_ACCELERATION: 1, // acceleration of player (x)
		PLAYER_SPEED: 11, // standard player x speed
		MAX_HEALTH: 100, // maximum health for player
		
		/** ARCHER **/
		
		PROJECTILE_DAMAGE: 10, // damage projectile performs
		PROJECTILE_VELOCITY: 24, // projectile's full power velocity
		PROJECTILE_CHARGETIME: 500, // time to fully power projectile launch
		
		/** GUARDSMAN **/
		
		SHIELD_SIZE: Math.PI/6, // size of a Guardsman's shield (1/2)
		ATTACKRATE_GUARDSMAN: 500, // attack cooldown of guardsman
		SHIELD_COOLDOWN: 300, // time shield is during for melee attack
		MELEE_DAMAGE: 20, // damage from melee attack
		
		/** GLOBAL METHODS **/
		
		getRelAngle: getRelAngle, // local function
		Extend: Extend,	// Extend an object -- Hacky Inheritance
		
		/** MISC **/
		CANVAS_WIDTH: 680, // width of client canvas
		CANVAS_HEIGHT: 480, // height of client canvas
	});
	
});