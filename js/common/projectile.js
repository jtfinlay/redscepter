if (typeof define !== 'function') {
	var define = require('../common/amdefine')(module);
}

/**************************************************
** PROJECTILE CLASS
**************************************************/

define(function(require) {

	var Info = require('../common/info');

	var Projectile = function(startX, startY, direction, velocity, id, sourceID) {
		
		this.incoming = [];
		
		// variables
		this.x = startX;
		this.y = startY;
		this.sourceID = sourceID || 0;
		this.id = id;
		
		// Unit vectors
		var ex = Math.cos(direction);
		var ey = Math.sin(direction);
		
		// Velocity
		this.dx = velocity * ex;
		this.dy = velocity * ey;
		
		this.size = 5;
		this.color = "#000000";
		this.alive = true;		
	}
	
	/**************************************************
	** GET
	**************************************************/

	Projectile.prototype.left = function() {
		return this.x;
	}
	Projectile.prototype.right = function() {
		return this.x + this.size;
	}
	Projectile.prototype.top = function() {
		return this.y;
	}
	Projectile.prototype.bottom = function() {
		return this.y + this.size;
	}
	Projectile.prototype.getCentre = function() {
		var x = this.x + this.size/2;
		var y = this.y + this.size/2;
		return (x,y);
	}
	
	/**************************************************
	** METHODS
	**************************************************/
	
	Projectile.prototype.update = function(players, map) {
		
		/** MOVEMENT **/
		
		// gravity
		this.dy += Info.GRAVITY;
		
		// terminal velocity
		this.dy = Math.min(this.dy, Info.TERMINAL_VELOCITY);
		
		// apply the velocities
		this.x += this.dx;
		this.y += this.dy;
		
		/** MAP OBSTACLES **/
		for (var i=0; i < map.objects.length; i++) {
			var obj = map.objects[i];
			
			// boundary box collision
			if (this.left() > obj.right) continue;
			if (this.top() > obj.bottom) continue;
			if (this.right() < obj.left) continue;
			if (this.bottom() < obj.top) continue;
			// Since everything is a box, we know we have a hit
			
			// Kill the projectile
			this.alive = false;
			return;
		}
		
		/** PLAYERS **/
		for (var i=0; i < players.length; i++) {
			var p = players[i];
			
			// ensure player is not the source
			if (p.id == this.sourceID) 
				continue;
				
			// check if player has shield
			if (p.type == 'guardsman' && p.shieldUp()) {
				var shield_box = p.getShield();
				
				// boundary box collision -- this is ugly, should refactor
				var hit = true;
				if (this.left() > shield_box.right) hit = false;
				if (this.top() > shield_box.bottom) hit = false;
				if (this.right() < shield_box.left) hit = false;
				if (this.bottom() < shield_box.top) hit = false;
				
				if (hit) {
					this.alive = false;
					return;
				}
			
			}
				
			// boundary box collision
			if (this.left() > p.right()) continue;
			if (this.top() > p.bottom()) continue;
			if (this.right() < p.left()) continue;
			if (this.bottom() < p.top()) continue;
			
			// since everything is a box, we have a hit
			
			this.alive = false;
			p.hurt(Info.PROJECTILE_DAMAGE);
			return;
		}
		
		/** WALLS **/
		if (this.left() < 0)
			this.alive = false;
		else if (this.right() > map.width)
			this.alive = false;
		else if (this.bottom() > map.height)
			this.alive = false;
	}
	
	// Client Update
	Projectile.prototype.ClientUpdate = function() {
	
		var currentTime = new Date().getTime();
		
		// Clean out old updates
		for (var i=0; i < this.incoming.length; i++) {
			if (currentTime > this.incoming[i].time) {
				this.incoming.splice(i, 1);
				i--;
			}
		}
		
		// Move projectile to different position
		if (this.incoming.length) {
			var nextStep = this.incoming[0];
			
			this.x = nextStep.x;
			this.y = nextStep.y;
			this.dx = nextStep.dx;
			this.dy = nextStep.dy;
		}
	}
	
	Projectile.prototype.draw = function(context) {
		context.fillStyle = this.color;
		context.fillRect(this.x, this.y, this.size, this.size);
	}
	
	
	/**************************************************
	** GATHER & EXTRACT DATA
	**************************************************/
	
	// puts all important info in a data package
	Projectile.prototype.gather = function() {
	
		var data = {
			id : this.id,
			x : this.x,
			y : this.y,
			color: this.color,
		};
		
		return data;	
	}
	
	// imports all important info from a data package
	Projectile.prototype.importData = function(data) {
	
		if (data.id) this.id = data.id;
		if (data.x) this.x = data.x;
		if (data.y) this.y = data.y;
		if (data.color) this.color = data.color;
	}
	
	// add update
	Projectile.prototype.addUpdate = function(time, data) {
		data.time = time;
		this.incoming.push(data);
	}
	
	return Projectile;
	
});
		
		
		