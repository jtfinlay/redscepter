
if (typeof define !== 'function') {
	var define = require('../common/amdefine')(module);
}

define(function(require) {


	/**************************************************
	** MAP CLASS
	**************************************************/
	function Map(mapId) {
			// mapId holds which map to load
			// It doesn't matter which id is sent right now, since there
			// is only a single map.
			
			// array of objects
			this.objects = [];
			
			// size of this map
			this.width = 1000; // canvas: 640
			this.height = 1000; // canvas: 480
			
			// add objects
			this.objects.push(new Rect(350, 825, 400, 850)); // floating 1
			this.objects.push(new Rect(480, 750, 530, 775)); // floating 2
			this.objects.push(new Rect(700, 700, 750, 725)); // floating 3
			this.objects.push(new Rect(350, 600, 650, 610)); // floating long
			this.objects.push(new Rect(480, 900, 560, 960)); // ground block
			this.objects.push(new Rect(0, 940, 1000,1000)); // the floor
			// this.objects.push(new Rect(0, 440, 680, 480));
			

	};

	Map.prototype.draw = function(context) {
			// draw the sky
			var _gradient = context.createLinearGradient(0, this.height, 0, 0);
			_gradient.addColorStop(0, "#d6eefa");
			_gradient.addColorStop(1, "e6eefa");
			context.fillStyle = _gradient;
			context.fillRect(0, 0, this.width, this.height);
			
			// draw objects
			for (var i = 0; i < this.objects.length; i++) {
					var o = this.objects[i];
					o.draw(context);
			};
	};


	/**************************************************
	** RECT (HELPER) CLASS
	**************************************************/
	function Rect(xi, yi, xf, yf) {
			this.left = xi;
			this.top = yi;
			this.right = xf;
			this.bottom = yf;
	}

	Rect.prototype.draw = function(context) {
			var width = this.right - this.left;
			var height = this.bottom - this.top;
			// fill with player colour
			context.fillStyle = "BurlyWood";
			context.fillRect(this.left, this.top, width, height);
			// outline with black border
			context.strokeStyle = "black";
			context.strokeRect(this.left, this.top, width, height);

	}
	
	return Map;
});