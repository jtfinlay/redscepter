
if (typeof define !== 'function') {
	var define = require('../common/amdefine')(module);
}

define(function(require) {

	var Info = require('../common/info'),
		Player = require('../common/player'),
		Inputs = require('../common/inputs');
		
	
	/**************************************************
	** ARCHER CLASS - INHERITS PLAYER
	**************************************************/
	
	var Archer = function(startX, startY, id) {
	
		// Inherits from Player
		Info.Extend(new Player(startX, startY, id), this);
	
		this.type = 'archer';
	};
	

	return Archer;

});