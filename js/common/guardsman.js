
if (typeof define !== 'function') {
	var define = require('../common/amdefine')(module);
}

define(function(require) {

	var Info = require('../common/info'),
		Player = require('../common/player'),
		Inputs = require('../common/inputs');
		
	
	/**************************************************
	** GUARDSMAN CLASS - INHERITS PLAYER
	**************************************************/
	
	var Guardsman = function(startX, startY, id) {
	
		// Inherits from Player
		Info.Extend(new Player(startX, startY, id), this);
		this.lastAttack = 0;
		this.type = 'guardsman';
	}
	
	/**************************************************
	** OVERRIDE METHODS
	**************************************************/

	//@Override
	Guardsman.prototype.getJump = function() {
		if (this.keys.spaceDown)
			return Info.JUMP_VELOCITY/2;
		else
			return Info.JUMP_VELOCITY;
	}
	// @Override
	Guardsman.prototype.setMisc = function() {
		
		var nextStep = this.incoming[0]; 
	
		// Check if attack info was sent
		if (nextStep.time && nextStep.attack) {
			// lastAttack is a boolean value, so use the state's time value
			this.lastAttack = nextStep.time;
		}
	}

	//@Override
	Guardsman.prototype.getXVeloc = function() {
		if (this.keys.spaceDown)
			return this.dx/2;
		else
			return this.dx;
	}
	
	//@Override
	Guardsman.prototype.draw = function(context) {
		
		/** PLAYER COLOUR **/
		context.fillStyle = this.color;
		context.fillRect(this.x, this.y, this.size, this.size);
	
		 /** BORDER **/
        context.strokeStyle = "black";
        context.strokeRect(this.x, this.y, this.size, this.size);
		
		/** ATTACK **/
		if (new Date().getTime() - this.lastAttack < 100)
			this.drawAttack(context);
		
		/** SHIELD **/
		// there is a cooldown when attacking where shield cannot be up
		if (this.keys.spaceDown && this.shieldUp())
			this.drawShield(context);
		
		/** USERNAME **/
		this.drawUsername(context);
		
		/** HEALTH BAR **/
		this.drawHealthBar(context);
		
		/** WRITE 'DED' ON PLAYER **/
		if (this.health <= 0) {
			context.fillStyle = "white";
			context.textAlign = "center";
			context.fillText("DED", this.getCentre().x, this.getCentre().y + 4);
		}
	}
	
	// Returns boolean of whether shield is up or no
	Guardsman.prototype.shieldUp = function() {
		if (!this.keys.spaceDown) return false;
		
		var timeCheck = (new Date().getTime() - this.lastAttack > Info.SHIELD_COOLDOWN)
		return timeCheck;
	}
	
	// Draw shield
	Guardsman.prototype.drawShield = function(context) {
		
		var angle = Info.getRelAngle(this.x, this.y, this.keys.mouseX, this.keys.mouseY);
		
		// set variables
		var x = this.getCentre().x,
			y = this.getCentre().y,
			inner_radius = this.size,
			outer_radius = inner_radius + 10,
			startAngle = angle - Info.SHIELD_SIZE,
			endAngle = angle + Info.SHIELD_SIZE;
		
		// colour
		context.fillStyle = "red";
		
		// create the path
		context.beginPath();
		context.arc( x, y, outer_radius, startAngle, endAngle, false);
		context.arc( x, y, inner_radius, endAngle, startAngle, true);
		context.closePath();
		
		// draw
		context.fill();

	}
	// Draw attack
	Guardsman.prototype.drawAttack = function(context) {
		
		var angle = Info.getRelAngle(this.x, this.y, this.keys.mouseX, this.keys.mouseY);
		
		// for now, the angle-range is the same as that of the shield
		var x = this.getCentre().x,
			y = this.getCentre().y,
			inner_radius = this.size - 10,
			outer_radius = this.size + 50,
			startAngle = angle - Info.SHIELD_SIZE,
			endAngle = angle + Info.SHIELD_SIZE;
			
		context.fillStyle = "yellow";
		
		context.beginPath();
		context.arc( x, y, outer_radius, startAngle, endAngle, false);
		context.arc( x, y, inner_radius, endAngle, startAngle, true);
		context.closePath();
		
		context.fill();		
		
	}
	
	// Return shield
	Guardsman.prototype.getShield = function() {
		// For now, just return a boundary box
		
		var angle = Info.getRelAngle(this.x, this.y, this.keys.mouseX, this.keys.mouseY);
		
		// list of points that could be limits
		var xs = [];
		xs.push((this.size)*Math.cos(angle-Info.SHIELD_SIZE));
		xs.push((this.size+10)*Math.cos(angle-Info.SHIELD_SIZE));
		xs.push((this.size+10)*Math.cos(angle));
		xs.push((this.size+10)*Math.cos(angle+Info.SHIELD_SIZE));
		xs.push((this.size)*Math.cos(angle+Info.SHIELD_SIZE));
		
		var ys = [];
		ys.push((this.size)*Math.sin(angle-Info.SHIELD_SIZE));
		ys.push((this.size+10)*Math.sin(angle-Info.SHIELD_SIZE));
		ys.push((this.size+10)*Math.sin(angle));
		ys.push((this.size+10)*Math.sin(angle+Info.SHIELD_SIZE));
		ys.push((this.size)*Math.sin(angle+Info.SHIELD_SIZE));
		
		// package it
		var data = {
			left: this.getCentre().x + Math.min.apply(null, xs),
			top: this.getCentre().y + Math.min.apply(null, ys),
			right: this.getCentre().x + Math.max.apply(null, xs),
			bottom: this.getCentre().y + Math.max.apply(null, ys)
		}
		
		return data;
	}
	// Return attack area
	Guardsman.prototype.getAttackArea = function() {
		// For now, just return a boundary box
		
		var angle = Info.getRelAngle(this.x, this.y, this.keys.mouseX, this.keys.mouseY);
		
		// list of points that could be limits
		var xs = [];
		xs.push((this.size)*Math.cos(angle-Info.SHIELD_SIZE));
		xs.push((this.size+50)*Math.cos(angle-Info.SHIELD_SIZE));
		xs.push((this.size+50)*Math.cos(angle));
		xs.push((this.size+50)*Math.cos(angle+Info.SHIELD_SIZE));
		xs.push((this.size)*Math.cos(angle+Info.SHIELD_SIZE));
		
		var ys = [];
		ys.push((this.size)*Math.sin(angle-Info.SHIELD_SIZE));
		ys.push((this.size+50)*Math.sin(angle-Info.SHIELD_SIZE));
		ys.push((this.size+50)*Math.sin(angle));
		ys.push((this.size+50)*Math.sin(angle+Info.SHIELD_SIZE));
		ys.push((this.size)*Math.sin(angle+Info.SHIELD_SIZE));
		
		// package it
		var data = {
			left: this.getCentre().x + Math.min.apply(null, xs),
			top: this.getCentre().y + Math.min.apply(null, ys),
			right: this.getCentre().x + Math.max.apply(null, xs),
			bottom: this.getCentre().y + Math.max.apply(null, ys)
		}
		
		return data;
	}
	
	return Guardsman;

});