if (typeof define !== 'function') {
	var define = require('../common/amdefine')(module);
}

/**************************************************
** USER INPUTS
**************************************************/

define(function(require) {

	var Info = require('../common/info');

	function Inputs() {
		// ensure all variables set
		this.up = false;
		this.left = false;
		this.right = false;
		this.mouseDown = false;
		this.spaceDown = false;
		// mouse location on the map
		this.mouseX = 0;
		this.mouseY = 0;
		// mouse location on the canvas
		this.canvas_mouseX = 0;
		this.canvas_mouseY = 0;
		
		this.timeDown = 0;
	};

	Inputs.prototype.onKeyDown = function(e) {
		var that = this,
			c = e.keyCode;
		
		switch(c) {
			case 37: case 65: // Left
				that.left = true;
				break;
			case 38: case 87: // Up
				that.up = true;
				break;
			case 39: case 68: // Right
				that.right = true;
				break;
			case 40: case 83: // Down
				that.down = true;
				break;
			case 32:		  // Space
				that.spaceDown = true;
				break;
		};
	};
		
	Inputs.prototype.onKeyUp = function(e) {
		var that = this,
			c = e.keyCode;
		
		switch(c) {
			case 37: case 65: // Left
				that.left = false;
				break;
			case 38: case 87: // Up
				that.up = false;
				break;
			case 39: case 68: // Right
				that.right = false;
				break;
			case 40: case 83: // Down
				that.down = false;
				break;
			case 32:		  // Space
				that.spaceDown = false;
				break;
		};
	};

	Inputs.prototype.onMouseMove = function(x,y, offset) {
		// get mouse coords
		this.mouseX = x;
		this.mouseY = y;
		this.canvas_mouseX = x;
		this.canvas_mouseY = y;
		
		// the view offset to convert canvas coords to map coords
		if (offset) {
			this.mouseX += offset.x;
			this.mouseY += offset.y;
		}
	};

	Inputs.prototype.onMouseUp = function(x,y, offset) {
		this.mouseDown = false;
		
		// get mouse coords
		this.mouseX = x;
		this.mouseY = y;
		this.canvas_mouseX = x;
		this.canvas_mouseY = y;
		
		// mouse is up - reset the time
		this.timeDown = 0;
		
		// the view offset to convert canvas coords to map coords
		if (offset) {
			this.mouseX += offset.x;
			this.mouseY += offset.y;
		}
	};

	Inputs.prototype.onMouseDown = function(x,y, offset) {
		this.mouseDown = true;
	
		// get mouse coords
		this.mouseX = x;
		this.mouseY = y;
		this.canvas_mouseX = x;
		this.canvas_mouseY = y;
		
		// mouse is down - set the time
		this.timeDown = new Date().getTime();
		
		// the view offset to convert canvas coords to map coords
		if (offset) {
			this.mouseX += offset.x;
			this.mouseY += offset.y;
		}
	};

	Inputs.prototype.onSpaceDown = function(e) {
		this.spaceDown = true;
	};
	
	Inputs.prototype.gather = function() {
		var data = {
			up : this.up,
			left : this.left,
			right : this.right,
			mouseDown : this.mouseDown,
			spaceDown : this.spaceDown,
			mouseX : this.mouseX,
			mouseY : this.mouseY
		}
		return data;
	};
	
	
	/**************************************************
	** SET
	**************************************************/
	
	Inputs.prototype.set = function(data) {	
		this.up = data.up || false;
		this.left = data.left || false;
		this.right = data.right || false;
		this.spaceDown = data.spaceDown || false;
		this.mouseDown = data.mouseDown || false;
		this.mouseX = data.mouseX || 0;
		this.mouseY = data.mouseY || 0;
		
		// Record time when mouse goes down
		if (this.mouseDown && !this.timeDown)
			this.timeDown = new Date().getTime();	
	}

	
	return Inputs;
});	
	
	
	