if (typeof define !== 'function') {
	var define = require('../common/amdefine')(module);
}

define(function(require) {

	var Info = require('../common/info'),
		Inputs = require('../common/inputs');
		

	/**************************************************
	** PLAYER CLASS
	**************************************************/

	var Player = function(startX, startY, id) {
		
		/** Updates from server **/
		this.incoming = [];
		
		// param variables
		this.x = startX || 0;
		this.y = startY || 0;
		this.id = id;
		
		this.keys = new Inputs();
		
		// default variables
		this.name = "Nameless";
		this.type = "none";
		this.size = 40;
		this.color = "#000000";
		this.health = Info.MAX_HEALTH;
		
		this.on_ground = false;
		this.dx = 0;
		this.dy = 0;
	};
	
	/**************************************************
	** GET
	**************************************************/

	Player.prototype.left = function() {
		return this.x;
	};
	Player.prototype.right = function() {
		return this.x + this.size;
	};
	Player.prototype.top = function() {
		return this.y;
	};
	Player.prototype.bottom = function() {
		return this.y + this.size;
	};
	Player.prototype.getCentre = function() {
		var _x = this.x + this.size/2;
		var _y = this.y + this.size/2;
		return {x:_x, y:_y};
	};
	Player.prototype.getJump = function() {
		return Info.JUMP_VELOCITY;
	};
	Player.prototype.getXVeloc = function() {
		return this.dx;
	};
	
	/**************************************************
	** keys
	**************************************************/
	
	// Keyboard key down
	Player.prototype.onKeyDown = function(e) {
		this.keys.onKeyDown(e);
	};

	// Keyboard key up
	Player.prototype.onKeyUp = function(e) {
		this.keys.onKeyUp(e);
	};

	// Mouse move
	Player.prototype.onMouseMove = function(x, y) {
		this.keys.onMouseMove(x, y);
	};

	// Mouse down
	Player.prototype.onMouseDown = function(x, y) {
		this.keys.onMouseDown(x, y);
	};

	// Mouse up
	Player.prototype.onMouseUp = function(x, y) {
		this.keys.onMouseUp(x, y);
	};
	
	/**************************************************
	** UPDATE
	**************************************************/

	// Server update
	Player.prototype.update = function(map) {
		
		/** FROM INPUT **/
		
		// right / left
			
		this.dx = 0;
		this.dx -= this.keys.left && Info.PLAYER_SPEED;
		this.dx += this.keys.right  && Info.PLAYER_SPEED;
		
		// displace
		this.x += this.getXVeloc();
		
				// up / jump
		if (this.keys.up && this.dy == 0 && this.on_ground) {
			// can only jump if on the ground
			this.dy = this.getJump();
			this.on_ground = false;
		}
		
		/** FROM ENVIRONMENT **/
		
		// Y movement
		this.dy += Info.GRAVITY;
		
		// Terminal velocity
		this.dy = Math.min(this.dy, Info.TERMINAL_VELOCITY);
		
		this.y += this.dy;
		
		/** ENSURE NOT OUT OF MAP **/
		if (this.y + this.size > map.height) {
			this.y = map.height - this.size;
			this.on_ground = true;
			this.dy = 0;
		}
		if (this.x < 0) {
			this.x = 0;
			this.dx = 0;
		}
		if (this.right() > map.width) {
			this.x = map.width - this.size;
			this.dx = 0;
		}
		
		/** MAP OBSTACLES**/
		for (var i=0; i<map.objects.length; i++) {
			var obj = map.objects[i];
			
			// check for boundarybox collision
			if (this.x > obj.right) continue;
			if (this.y > obj.bottom) continue;
			if (this.right() < obj.left) continue;
			if (this.bottom() < obj.top) continue;
                
			// We have a collision. How deep into the layer is the player?
            
			var rightAmt    = this.right() - obj.left;
			var leftAmt     = obj.right - this.x;
			var bottomAmt   = this.bottom() - obj.top;
			var topAmt      = obj.bottom - this.y;
            
			// taking min, since the min is the collision distance
			var xdeep = Math.min( rightAmt, leftAmt );
			var ydeep = Math.min( bottomAmt, topAmt );
                
			// whichever is shallowest is the collision we care about
			if (xdeep < ydeep) {
				this.x += (rightAmt > leftAmt) ? leftAmt : -rightAmt;
			} else {
				if (bottomAmt > topAmt) { 
					// hitting top of obstacle
					this.y += topAmt;
					this.dy = 0;
				} else if (this.dy >= 0) {
					// hitting bottom of obstacle
					this.y -= bottomAmt;
					this.on_ground = true;
					this.dy = 0;                   
				}            
			}
		}
	};
	
	// Client update
	Player.prototype.ClientUpdate = function() {
		
		var currentTime = new Date().getTime();
		
		// Clean out old updates
		var kill_counter = 0;
		for (var i=0; i<this.incoming.length; i++) {
			if (currentTime > this.incoming[i].time) {
				// ensure no loss of misc data, such as melee attacks
				this.setMisc();
				
				this.incoming.splice(i,1);
				i--;
			}
		}
		
		// Choose update type
		if (this.incoming.length) {
			// if there are states available, interpolate for motion smoothing
			this.Interpolate();
			
			// set any misc attributes
			this.setMisc();
		}
		
		
	}
	
	// Client interpolation
	Player.prototype.Interpolate = function() {
		
		var currentTime = new Date().getTime();
		
		/** LAGRANGE POLYNOMIAL: **/
		// Lagrange can be found in previous commits
		
		
		var nextStep = this.incoming[0];
		
		this.x = nextStep.x;
		this.y = nextStep.y;
		this.dx = nextStep.dx;
		this.dy = nextStep.dy;
		this.health = nextStep.health;
		this.setInputs(nextStep.inputs);
		
	}
	
	// Misc update
	Player.prototype.setMisc = function() {
		// nothing here on default player. This method is extended
		// by the classes. For example, Guardsman uses this method
		// to check for attacks
	}
	
	/**************************************************
	** DRAWING
	**************************************************/

	// draw player to canvas
	Player.prototype.draw = function(context) {

		/** PLAYER COLOUR **/
		context.fillStyle = this.color;
		context.fillRect(this.x, this.y, this.size, this.size);
	
		 /** BORDER **/
        context.strokeStyle = "black";
        context.strokeRect(this.x, this.y, this.size, this.size);
		
		/** USERNAME **/
        this.drawUsername(context);
		
		/** HEALTH BAR **/
		this.drawHealthBar(context);
		
		/** WRITE 'DED' ON PLAYER **/
		if (this.health <= 0) {
			context.fillStyle = "white";
			context.textAlign = "center";
			context.fillText("DED", this.getCentre().x, this.getCentre().y + 4);
		}
	};

	// Draws this.name under the player
	Player.prototype.drawUsername = function(context) {
			context.fillStyle = "black";
			context.textAlign = "center";
			context.font = "bold 16px Arial";
			var text = this.name;
			if (text.length > 12) text = text.substring(0,11) + "..";
			context.fillText(text, this.getCentre().x, this.y + this.size + 15);
	};
	
	// Draws this.health over the player
	Player.prototype.drawHealthBar = function(context) {
		
		var amt = this.health / Info.MAX_HEALTH;
		
		context.beginPath();
		context.moveTo(this.x, this.y -5);
		context.lineTo(this.x + (amt*this.size), this.y -5);
		context.closePath();
		context.lineWidth = "4";
		context.strokeStyle = "green";
		context.stroke();
		
		context.beginPath();
		context.moveTo(this.x + (amt*this.size), this.y -5);
		context.lineTo(this.x + this.size, this.y -5);
		context.closePath();
		context.lineWidth="4";
		context.strokeStyle = "red";
		context.stroke();
		
		// reset linewidth
		context.lineWidth = "1";
		
	}

	/**************************************************
	** MISC
	**************************************************/

	// Something has hurt the player. So sad :'(
	Player.prototype.hurt = function(amt) {
		this.health -= amt;
	
		// dead?
		if (this.health < 0) this.health = 0;
	}
	
	/**************************************************
	** GATHER & EXTRACT DATA
	**************************************************/

	// puts all important info in a data package
	Player.prototype.gather = function() {
		
		var data = {
			id : this.id,
			type: this.type,
			x : this.x,
			y : this.y,
			inputs: this.keys.gather(),
			color : this.color,
			name : this.name,
			health: this.health
		}
		
		return data;
	}
	

	// imports all important info from a data package
	Player.prototype.importData = function(data) {
		
		if (data.id) this.id = data.id;
		if (data.color) this.color = data.color;
		if (data.name) this.name = data.name;
		if (data.type) this.type = data.type;
		if (data.x) this.x = data.x;
		if (data.y) this.y = data.y;
		if (data.health) this.health = data.health;
		if (data.inputs) this.setInputs(data.inputs);

	};
	
	// add update
	Player.prototype.addUpdate = function(time, data) {
		// add time
		data.time = time;
		this.incoming.push(data);
		
		// sort states
		this.incoming.sort(function(a,b){return a.time-b.time});
	}
	
	// set this player's input status
	Player.prototype.setInputs = function(data) {
		this.keys.set(data);		
	};
	
	


	return Player;

});