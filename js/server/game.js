requirejs.config({
	baseUrl: './server'
});
	
/**************************************************
** GAME CLASS
**************************************************/
define(
function(require) {
	
	/** IMPORT FILES **/
	var Info = require('../common/info');
	var Archer = require('../common/archer');
	var Guardsman = require('../common/guardsman');
	var Map = require('../common/map');
	var Projectile = require('../common/projectile');
	
	/** Holds all game states.
	 *
	 * @remarks
	 *    Any object older than Info.UPDATE_STORAGE_PERIOD will be removed
	 *   from this array
	 */
	var _states = {};
	
	// Creation
	var Game = function() {

		// Players
		this.allPlayers = [];
		this.projectiles = [];
		this.map = new Map(0);

	};

	/**************************************************
	** PLAYER METHODS
	**************************************************/

	// Add a new player to the game
	Game.prototype.AddPlayer = function(data) {
		
		// Create object
		var newPlayer;
		
		if (data.type == 'archer')
			newPlayer = new Archer(40, 40, data.id);
		else if (data.type == 'guardsman')
			newPlayer = new Guardsman(40, 40, data.id);
		else  {
			console.log("Invalid player type: " + data.type);
			return;
		}
		
		if (data.name) newPlayer.name = data.name;
		if (data.color) newPlayer.color = data.color;
		
		// Add to all players
		this.allPlayers.push(newPlayer);

	};

	// Player has sent inputs
	Game.prototype.PlayerInputs = function(data) {
		
		var player = this.playerById(data.id);
		
		if (!player) {
			console.log("Player not found: " + data.id);
			return;
		};
		
		// update player
		player.setInputs(data.inputs);		
		
	
		if (player.type == 'archer') {
			// if player is an archer, we need to watch for the mouse
			// being released
			if (!player.keys.mouseDown && player.keys.timeDown) {
				
				this.launchProjectile(player);
				
			}
		} else if (player.type == 'guardsman') {
			// if player is a guardsman, we need to watch for the mouse
			// being down for attacks
			var currentTime = new Date().getTime();
			var timeCheck = currentTime - player.lastAttack >= Info.ATTACKRATE_GUARDSMAN;
			if (player.keys.mouseDown && timeCheck) {
				
				this.meleeAttack(player);
				
			}
		}
	};

	// Remove player from the game
	Game.prototype.RemovePlayer = function(id) {
		
		var player = this.playerById(id);
		
		if (!player) {
			console.log("Tried to remove player, but not found: " + id);
			return;
		}
		
		// add remove event
		var time = new Date().getTime();
		var data = {};
		data.player = [];
		data.player.push({
				id: id,
				remove: true
		});
		this.addState(time, data);
		
		// remove player from array
		this.allPlayers.splice(this.allPlayers.indexOf(player), 1);
	};
	
	/**************************************************
	** SPECIAL KEY EVENT METHODS
	**************************************************/
	
	/** Logic for player to launch projectile **/
	Game.prototype.launchProjectile = function(player) {
		// how long mouse was down for
		var dt = new Date().getTime() - player.keys.timeDown; 
				
		// power ratio
		var ratio = (dt / Info.PROJECTILE_CHARGETIME);
		if (ratio > 1) ratio = 1;
		
		// velocity to fire projectile
		var power = ratio * Info.PROJECTILE_VELOCITY;
		
		// get angle to fire projectile
		var mx = player.keys.mouseX,
			my = player.keys.mouseY,
			x = player.getCentre().x,
			y = player.getCentre().y;
		var angle = Info.getRelAngle(x,y,mx,my);
		
		// create projectile object
		var projectile = new Projectile(x, y, angle, power, this.generateID(), player.id); 
		
		// push projectile to list
		this.projectiles.push(projectile);
		
		// reset time
		player.keys.timeDown = 0;
	}
	
	/** Logic for player to perform melee swing **/
	Game.prototype.meleeAttack = function(player) {
		player.lastAttack = new Date().getTime();
		
		// variables
		var mx = player.keys.mouseX,
			my = player.keys.mouseY,
			x = player.getCentre().x,
			y = player.getCentre().y, 
			angle = Info.getRelAngle(x,y,mx,my);
		
		// alert players of attack
		var time = new Date().getTime();
		var message = {};
		message.player = [];
		var state = player.gather();
		state['attack'] = true;
		message.player.push(state);
				
		this.addState(time, message);
		
		// melee attack logic
		for (var i=0; i<this.allPlayers.length; i++) {
			var p = this.allPlayers[i];
			
			if (p.id == player.id) continue;
		
			var box = player.getAttackArea();
			
			if (box.left > p.right()) continue;
			if (box.top > p.bottom()) continue;
			if (box.right < p.left()) continue;
			if (box.bottom < p.top()) continue;
			
			// we have a hit!
			p.hurt(Info.MELEE_DAMAGE);
		}
	}
	

	/**************************************************
	** UPDATE
	**************************************************/

	/** Updates all objects in the game
	 * 
	 *
	 * @remarks
	 *    1) The Updates array is purged of any old data.
	 *	  2) Players are updated. If any important changes are made, they are
	 *   to the Updates array.
	 * 
	 */
	Game.prototype.update = function() {
		
		/** CLEAN _STATES ARRAY **/
		for (var param in _states) {
			// clean out any old updates
			var dt = new Date().getTime() - param;
			if (dt > Info.UPDATE_STORAGE_PERIOD)
				delete _states[param];
		};			
		
		/** REMOVE DEAD PROJECTILES **/
		for (var i=0; i < this.projectiles.length; i++) {
			var p = this.projectiles[i];
			if (!p.alive) {
				// add remove event
				var time = new Date().getTime();
				
				var message = {};
				message.projectile = [];
				message.projectile.push({id: p.id, remove: true});
				this.addState(time, message);
		
				// remove from array
				this.projectiles.splice(i, 1);
				i--;
			}
		}
		
		/** PLAYERS **/
		for (var i=0; i < this.allPlayers.length; i++) {
			var _player = this.allPlayers[i];
			// update the player
			_player.update(this.map);
		};
		
		/** PROJECTILES **/
		for (var i=0; i < this.projectiles.length; i++) {
			var p = this.projectiles[i];
			p.update(this.allPlayers, this.map);
		}
		
		/** ADD DATA TO STATES ARRAY **/
		var time = new Date().getTime();
		this.addState(time, this.getGameState());
	};

	/**************************************************
	** STATES & GATHER DATA
	**************************************************/

	/** Adds a new state. Merges if state already exists
	 *
	 *	@remarks
	 * 		 This method is long because of the potential height of the structure, and
	 * 		because the structure could be undefined at any point. At it's largest,
	 * 		the state is as follows, where '{' denotes associate array, and '[' 
	 * 		denotes standard array :
	 *			_STATES = {
	 *				TIMESTAMP = {
	 *					TYPE = [
	 *						OBJECT = {
	 *							ID
	 *
	 *		 If the ID between the new value and an existing value matches, then we are updating
	 * 		an object twice in the same millisecond (yes, possible). The newest has priority. 
	 * 		However, the old methods are copied over as well in the event there are important
	 * 		actions that need to be transmitted to the user.
	 *
	 * 		 To make life more difficult, there could be any number of values in the standard
	 * 		arrays, and in most associate arrays.
	 **/
	Game.prototype.addState = function(time, data) {
		
		if (! _states[time]) {
			/** TIMESTAMP **/
			_states[time] = data;
			return;
		}
		
		for (var type in data) {
			if (!_states[time][type]) {
				/** TYPE **/
				_states[time][type] = data[type];
				continue;
			}
			for (var di in data[type]) {
				var found = false;
				for (var si in _states[time][type]) {
					if (data[type][di].id == _states[time][type][si].id) {
						found = true;
						/** OBJECT.ID -- Add/overwrite all old values with new **/
						var result = {};
						for (var param in data[type][di])
							_states[time][type][si][param] = data[type][di][param];
						continue;
					}
				}
				if (!found) {
					/** OBJECT.ID does not match. Just push it **/
					_states[time][type].push(data[type][di]);
				}
			}
		}
	}
	
	// return all states since given time
	Game.prototype.getStates = function(time) {
		var result = {}
		for (var key in _states) {
			if (key >= time)
				result[key] = _states[key];
		}
		return result;	
	}
	
	/** Returns information of all objects in current game state
	 * 
	 *
	 * @return
	 *    An associate array containing the updates. The structure of this
	 *   array is in form of data[type] = {...}. Where the structure of data[type]
	 *   is a standard array.
	 * 
	 */
	Game.prototype.getGameState = function() {

		/** CREATE COLLECTION **/
		var data = {};
		data['player'] = [];
		data['projectile'] = [];

		/** ADD PLAYER INFO **/
		for (var i=0; i < this.allPlayers.length; i++) {
			var p = this.allPlayers[i];
			data['player'].push(p.gather());
		};
		
		/** ADD PROJECTILE INFO **/
		for (var i=0; i < this.projectiles.length; i++) {
			var p = this.projectiles[i];
			data['projectile'].push(p.gather());
		};

		return data;
	};
	

	/**************************************************
	** HELPER METHODS
	**************************************************/

	// Get player by their id
	Game.prototype.playerById = function(id) {
		
		for (var i=0; i<this.allPlayers.length; i++) {
			if (this.allPlayers[i].id == id)
				return this.allPlayers[i];
		};
		
		return false;
		
	};
	
	// Generate new id
	Game.prototype.generateID = function() {
		var randomnumber = Math.floor(Math.random()*100001)
		
		// if the number matches any existing IDs, create a new one
		
		for (var i=0; i < this.allPlayers.length; i++)
			if (randomnumber == this.allPlayers[i].id)
				return this.generateID();
				
		for (var i=0; i< this.projectiles.length; i++)
			if (randomnumber == this.projectiles[i].id)
				return this.generateID();
		
		return randomnumber;	
	}
	
	return Game;
});