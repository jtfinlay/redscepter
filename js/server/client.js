/**************************************************
** CLIENT OBJECT 
**************************************************/

define(function() {

	var Client = function(id) {
		this.lastUpdate = -1;
		this.id = id;
	};

	return Client;

});