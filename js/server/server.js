/**************************************************
** SERVER CODE
**************************************************/

var requirejs = require("requirejs");
requirejs.config({
	nodeRequire: require
});

requirejs(['util', './client', 'socket.io', './game', '../common/info'],
function(util, Client, io, gamejs, Info) {

	var Game = new gamejs();
		
	var socket,
		clients;

	/**************************************************
	** START CONNECTION
	**************************************************/

	// initialize the data & start the server
	function start(port) {

		// initialize clients as empty array
		clients = [];
		
		// list on given port
		socket = io.listen(port);
		
		// configure socket
		socket.configure(function() {
			socket.set("transports", ["websocket"]);
			socket.set("log level", 2);
		});
		
		// wire events
		setEventHandlers();
		
		// start game loop
		run();
	};

	// event wiring
	var setEventHandlers = function() {
		// listen for new connections. Sends them to onSocketConnection
		socket.sockets.on("connection", onSocketConnection);
	}

	// connect to
	start(8000);

	/**************************************************
	** EVENTS
	**************************************************/

	// client has connected
	function onSocketConnection(client) {
		
		util.log("New client has connected: " + client.id);
		
		// Wire events
		client.on("disconnect", onClientDisconnect);
		client.on("new client", onNewClient);
		client.on("update client", onUpdateClient);
		
		// Tell client their id. @TODO: Actual map logic
		client.emit("setup", {
			id: client.id,
			mapID: 0
		});
	}

	// client has been added
	function onNewClient(data) {

		// create new client 
		var _client = new Client(this.id);
		
		data.id = this.id;
		Game.AddPlayer(data);
		
		// tell user to start game & send game state
		var message = {0: Game.getGameState()};
		this.emit("start", message);
		_client.lastUpdate = new Date().getTime();
		
		// add new player to game state
		clients.push(_client);
	};

	// client sends update (inputs)
	function onUpdateClient(data) {
		
		var currentTime = new Date().getTime();
		
		/** GET CLIENT **/
		var _client = clientById(this.id);
		
		if (!_client) {
			util.log("Tried to update client, but not found: " + _client.id);
			return;
		};
		
		/** SEND UPDATE INFO TO GAME **/
		Game.PlayerInputs(data);
		
		/** RESPOND TO CLIENT WITH NEW UPDATES **/
		var message = Game.getStates(_client.lastUpdate);
		message["timeSent"] = data.timeSent;
		message["timeReturned"] = new Date().getTime();
	
		this.emit("update", message);
			
		/** TRACK LAST UPDATE OF CLIENT **/
		_client.lastUpdate = currentTime;
		
	};

	// Client has disconnected
	function onClientDisconnect() {
		
		util.log("Client has disconnected: " + this.id);
		
		/** GET CLIENT **/
		var _client = clientById(this.id);
		
		if (!_client) {
			util.log("Tried to remove client, but not found: " + this.id);
			return;
		};
		
		/** Remove player **/
		Game.RemovePlayer(this.id);
		
		/** Remove client from clients array **/
		clients.splice(clients.indexOf(_client), 1);
		
	};

	/**************************************************
	** GAME LOOP
	**************************************************/

	// game loop
	function run() {

		setInterval(function() {
			update();
		
		}, Info.UPDATE_SERVER);
		
	};

	/**************************************************
	** UPDATE METHODS
	**************************************************/

	// update all objects
	function update() {
		Game.update();
	};

	/**********************************
	** HELPER FUNCTIONS
	**********************************/

	// Get client by their id
	function clientById(id) {
		
		for (var i=0; i<clients.length; i++) {
			if (clients[i].id == id)
				return clients[i];
		};
		
		return false;
	};
});



