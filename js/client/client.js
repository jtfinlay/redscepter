requirejs.config({
	// By default load any modules from js/client
	baseUrl: 'js/client',
});

define(
function(require) {

	var Info = require("../common/info");
	var Inputs = require("../common/inputs");
	var Archer = require("../common/archer");
	var Guardsman = require('../common/guardsman');
	var Projectile = require('../common/projectile');
	var io = require("http://localhost:8000/socket.io/socket.io.js");
	var Mapjs = require('../common/map');
	var Viewjs = require('../client/view');
	
	/**************************************************
	** CLIENT VARIABLES
	**************************************************/
	var canvas,		// HTML canvas
		context,	// Rendering context
		objects,	// players / projectiles
		id,			// client id
		socket,		// socket connection
		inputs,		// contians this client's keypresses
		map,		// the map for current game instance
		view,		// View that follows player around map
		running = false; // whether game is running
		

	/**************************************************
	** INITIALIZATION
	**************************************************/

	/** Client initialization */
	function init() {
		
		// Declare the canvas & context
		canvas = document.getElementById("myCanvas")
		canvas.onselectstart = function() { return false; }
		context = canvas.getContext("2d");
		
		// Canvas size
		canvas.width = Info.CANVAS_WIDTH;
		canvas.height = Info.CANVAS_HEIGHT;
		
		// inputs
		inputs = new Inputs();
		
		// socket connection
		socket = io.connect("http://localhost", {
			port: 8000,
			transports: ["websocket"]
		});
		
		// wire events
		setEventHandlers();
	};

	/**************************************************
	** WIRE EVENTS
	**************************************************/
		
	/** Wire the event handlers for Window & Socket*/
	var setEventHandlers = function() {

		// Keyboard
		window.addEventListener("keydown", onKeydown, false);
		window.addEventListener("keyup", onKeyup, false);
		
		// Mouse
		window.addEventListener("mousemove", onMouseMove, false);
		window.addEventListener("mousedown", onMouseDown, false);
		window.addEventListener("mouseup", onMouseUp, false);
		
		// Socket
		socket.on("connect", onSocketConnected);
		socket.on("disconnect", onSocketDisconnect);
		socket.on("setup", onSetupGame);
		socket.on("start", onStartGame);
		socket.on("update", onServerUpdate);
	};

	/**************************************************
	** SOCKET EVENTS
	**************************************************/

	/** Client has found and connected to socket server */
	function onSocketConnected() {
		console.log("Connected to socket server");
	};

	/** Client has received 'setup' message from server
	 * 
	 *
	 * @param data
	 *    Data block sent by server as associative array. Datablock contains:
	 *    	id: Client id on server
	 *    	mapID: id number of map being joined
	 * 
	 * @remarks
	 * 		Sets local player information and relevant game controls
	 * 
	 */
	function onSetupGame(data) {
		
		console.log("Setup - ID: " + data.id);
		
		id = data.id;
		map = new Mapjs(data.mapID);
		view = new Viewjs(map);
		
		// @TODO: Player selects name & class & (eventually) game instance
		var name = "Boris the Blade";
		var color = randomColor();
		// var type = 'archer';
		var type = 'guardsman';
		
		socket.emit("new client", {
			name: name,
			type: type,
			color: color
		});

	};

	/** Client has received 'start' message from server
	 * 
	 *
	 * @param data
	 *    Data block sent by server as associative array. Datablock contains:
	 *    	0: GameState
	 * 
	 */
	function onStartGame(data) {
		
		console.log("Start");
		
		// set objects & states
		objects = [];
		
		// Set game state
		onServerUpdate(data);
				
		// Start game loop
		running = true;
		run();
	};

	// This client has disconnected
	function onSocketDisconnect() {
		console.log("disconnected from socket server");
		running = false;
	};

	/**************************************************
	** INPUT EVENTS
	**************************************************/

	// Keyboard key down
	function onKeydown(e) {
		inputs.onKeyDown(e);
	};

	// Keyboard key up
	function onKeyup(e) {
		inputs.onKeyUp(e);
	};

	// Mouse move
	function onMouseMove(e) {
		if (view) {
			var c = relMouseCoords(e);
			var offset = {x: view.left, y: view.top};
			inputs.onMouseMove(c.x, c.y, offset);
		}
	};

	// Mouse down
	function onMouseDown(e) {
		if (view) {
			var c = relMouseCoords(e);
			var offset = {x: view.left, y: view.top};
			inputs.onMouseDown(c.x, c.y, offset);
		}
	};

	// Mouse up
	function onMouseUp(e) {
		if (view) {
			var c = relMouseCoords(e);
			var offset = {x: view.left, y: view.top};
			inputs.onMouseUp(c.x, c.y, offset);
		}
	};
	
	/**************************************************
	** UPDATES FROM SERVER
	**************************************************/
	
	/** Client has received 'update' message from server
	 *
	 *	@param data
	 *		Data block sent by server as associative array. Data contains:
	 *			[timestamp].[type] = <array of object info>
	 *	
	 *	@remarks 
	 *		Currently, the available types are: 'projectile' and 'player'
	 */
	function onServerUpdate(data) {
		/** PING (ROUND TRIP) **/
		var currentTime = new Date().getTime();
		var ping = currentTime - data.timeSent;
		
		var new_objects = [];
				
		/** GO THROUGH STATES **/
		for (var timestamp in data) {
			
			// Convert the given time into what we predict is the local time equivalent.
			// We assume package was sent ping/2 ms ago.
			var age = ping/2 + data.timeReturned - timestamp;
			var ourTime = currentTime + age + Info.TIME_DELAY;
			
			for (var type in data[timestamp]) {
				
				if (type == "player") {
				
					/** Player update **/
					var new_players = playerUpdate(data[timestamp][type], ourTime);
					for (var p in new_players)
						new_objects.push(new_players[p]);
						
				} else if (type == "projectile") {

					/** Projectile update **/
					var new_projectiles = projectileUpdate(data[timestamp][type], ourTime);
					for (var p in new_projectiles)
						new_objects.push(new_projectiles[p]);
						
				}				
			}
		}
		
		/** PUSH NEW OBJECTS **/
		for (var i=0; i<new_objects.length; i++)
			objects.push(new_objects[i]);
	}
	
	/** Update containing player info received from server
	 *
	 *	@param players
	 *		Array containing player update information.
	 *	@param ourTime
	 *		Time in millis data received, in client time
	 *	
	 *	@remarks
	 *		The items in the players array contains the information packaged in the 'player' object's
	 *		gather() method. Also may contain extra player info inserted by the server.
	 *
	 */
	function playerUpdate(players, ourTime) {
		
		// method returns any new players
		var new_players = [];
		
		for (var i=0; i<players.length; i++) {
			var dataBlock = players[i];
	
			/** GET PLAYER **/
			var player = objectById(dataBlock.id)
			
			if (dataBlock.remove) {
				/** REMOVE PLAYER **/
				removeObject(dataBlock.id)
				continue;
			}
			
			if (!player) {
				/** NEW PLAYER **/
				if (dataBlock.type == 'archer')
					player = new Archer();
				else if (dataBlock.type == 'guardsman')
					player = new Guardsman();
				else { console.log("No class defined."); continue; }
				
				player.importData(dataBlock);
				new_players.push(player);
				continue;
			}
			
			/** UPDATE PLAYER **/
			player.addUpdate(ourTime, dataBlock);
		}
		
		return new_players;
	}
	
	
	/** Update containing projectile info received from server
	 *
	 *	@param projectiles
	 *		Array containing projectiles update information.
	 *	@param ourTime
	 *		Time in millis data received, in client time
	 *	
	 *	@remarks
	 *		The items in the projectiles array contains the information packaged in the 'projectile' object's
	 *		gather() method. Also may contain extra projectile info inserted by the server.
	 *
	 */
	function projectileUpdate(projectiles, ourTime) {
		var new_projectiles = [];
		
		for (var i=0; i < projectiles.length; i++) {
			var dataBlock = projectiles[i];
		
			/** Get Projectile **/
			var projec = objectById(dataBlock.id);
			
			if (dataBlock.remove) {
				/** Remove Projectile **/
				removeObject(dataBlock.id)
				continue;
			}

			if (!projec && !dataBlock.remove) {
				/** New Projectile **/
				projec = new Projectile();
				projec.importData(dataBlock);
				new_projectiles.push(projec);
				continue;						
			}
			
			/** Update Projectile **/
			projec.addUpdate(ourTime, dataBlock);
		}
		
		return new_projectiles;
	}
	
	/**************************************************
	** GAME LOOP
	**************************************************/

	// game loop
	function run() {

		// UPDATE OBJECTS
		setInterval(function() {
			if (running) 
				update();
			draw();
		}, Info.UPDATE_DRAW);
		
		// TALK TO SERVER
		setInterval(function() {
			if (running) {
				send_update();
			}
		}, Info.UPDATE_REQUEST);
	};

	// emit update
	function send_update() {
		// This client sends key information to server. The server
		// will answer with an 'update' call with new game info.
	
		var currentTime = new Date().getTime();
		
		var data = {
			id: id,
			inputs: inputs.gather(),
			timeSent: currentTime
			};
		
		socket.emit("update client",  data);
	};
	
	// update
	function update() {	
		/** UPDATE OBJECTS **/
		for (var i=0; i<objects.length; i++) {
			objects[i].ClientUpdate();
		}	
		
		/** UPDATE VIEW **/
		var localPlayer = getLocalPlayer();
		var _x = localPlayer.getCentre().x,
			_y = localPlayer.getCentre().y;
		view.update(_x, _y);
	};

	/**************************************************
	** DRAW
	**************************************************/

	function draw() {

		var local = getLocalPlayer();
		
		/** Clear screen **/
		context.clearRect(0, 0, canvas.width, canvas.height);
		
		/** TRANSLATE CONTEXT FOR VIEW **/
		context.save();
		context.translate(-view.left, -view.top);
		
		/** Draw Map **/
		map.draw(context);
		
		/** Draw objects **/
		for (var i=0; i < objects.length; i++) {
			if (objects[i].id == local.id) continue; // skip local player
			objects[i].draw(context);
		}
		
		/** Draw Local Player **/
		local.draw(context); // draw after so on top of other players
		
		/** RESTORE CONTEXT FROM VIEW **/
		context.restore();
		
		/** Draw mouse pointer **/
		drawMousePointer(context);
		
		/** Draw Projectile Charge **/
		if (getLocalPlayer().type == 'archer')
			drawChargeCircle(context);

	};
	
	// draw the mouse pointer on the canvas
	function drawMousePointer(context) {
		var x = inputs.canvas_mouseX,
			y = inputs.canvas_mouseY;
		context.beginPath()
        context.moveTo(x-7, y);
        context.lineTo(x+7, y);
        context.moveTo(x, y+7);
        context.lineTo(x, y-7);
        context.closePath();
        
        context.strokeStyle = "black";
        context.stroke();
	}
	
	// draws charge around the mouse when held down
	function drawChargeCircle(context) {
		
		// mouse must be down
		if (!inputs.mouseDown) return;
		if (!inputs.timeDown) return;
		
		// mouse was previously down, update power
		var dt = new Date().getTime() - inputs.timeDown
		var ratio = dt / Info.PROJECTILE_CHARGETIME;
		if (ratio > 1) ratio = 1; // don't allow supercharging
	
		// draw the charger around mouse
		var x = inputs.canvas_mouseX;
		var y = inputs.canvas_mouseY;
		var inner_radius = 15;
		var outer_radius = 20;
		var startAngle = 0;
		var endAngle = 2*Math.PI * ratio;
		
		// colour
		if (ratio < .75)	context.fillStyle = 'red';
		else				context.fillStyle = 'blue';
		
		// create the path
		context.beginPath();
		context.arc(x, y, outer_radius, startAngle, endAngle, false); // outer (filled)
		context.arc(x, y, inner_radius, endAngle, startAngle, true); // inner (unfills centre)
		context.closePath();
		
		// draw
		context.fill();
	}


	/**************************************************
	** GAME HELPER FUNCTIONS
	**************************************************/

	// Get object by their id
	function objectById(id) {
		
		for (var i=0; i<objects.length; i++) {
			if (objects[i].id == id)
				return objects[i];
		};
		
		return false;
		
	};
	
	// remove object by id
	function removeObject(id) {
	
		for (var i=0; i<objects.length; i++) {
			if (objects[i].id == id) {
				objects.splice(i,1);
				break;
			}
		}
	
	};

	// Generate random hex color
	function randomColor() {
		var color = '#'+Math.floor(Math.random()*16777215).toString(16);
		while (color.length < 7) color += "0";
		return color;
	}

	// Get the mousecoords from top-left of canvas
	function relMouseCoords(event) {
		var totalOffsetX = 0;
		var totalOffsetY = 0;
		var canvasX = 0;
		var canvasY = 0;
		var currentElement = canvas;

		do{
			totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
			totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
		}
		while(currentElement = currentElement.offsetParent)

		canvasX = event.pageX - totalOffsetX;
		canvasY = event.pageY - totalOffsetY;	

		return {x:canvasX, y:canvasY}
	}
	
	function getLocalPlayer() {
		var o = objectById(id);
		if (!o)
			console.log("Local player not found: " + id);
		return o;
	}
	
	return {init: init};

});