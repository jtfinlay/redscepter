if (typeof define !== 'function') {
	var define = require('../common/amdefine')(module);
}

/**************************************************
** VIEW CLASS
**************************************************/

define(function(require) {

	var Info = require('../common/info');

	/** View used by client to follow localPlayer around map
	 *
	 * @param: map
	 *     Map object currently in use.
	 */
	var View = function(map) {
		if (map.width < Info.CANVAS_WIDTH)
			console.log("Warning: canvas is wider than the map!");
		if (map.height < Info.CANVAS_HEIGHT)
			console.log("Warning: canvas is taller than the map!");
		
		this.map = map;
		
		// the view acts as a drawing box.
		this.left = 0;
		this.right = Info.CANVAS_WIDTH;
		this.top = 0;
		this.bottom = Info.CANVAS_HEIGHT;	
	};
	
	/**************************************************
	** UPDATE
	**************************************************/

	/** Update the View
	 *
	 * @param: x, y
	 *     Position of player in terms of map coordinates
	 */
	View.prototype.update = function(x, y) {
		
		// Displace
        this.left = x - Info.CANVAS_WIDTH/2;
        this.right = this.left + Info.CANVAS_WIDTH;
        this.top = y - Info.CANVAS_HEIGHT/2;
        this.bottom = this.top + Info.CANVAS_HEIGHT;
		
		this.check();
	}
	
	/** Affirm that the View position does not exceed the map dimensions **/
	View.prototype.check = function() {
	    
        // Check boundaries
		if (this.left < 0) {
			this.left = 0;
			this.right = Info.CANVAS_WIDTH;
		}
		if (this.right > this.map.width) {
			this.left = this.map.width - Info.CANVAS_WIDTH;
			this.right = this.map.width;         
		}
		if (this.top < 0) {
			this.top = 0;
			this.bottom = Info.CANVAS_HEIGHT;
		}
		if (this.bottom > this.map.height) {
			this.bottom = this.map.height;
			this.top = this.map.height - Info.CANVAS_HEIGHT;
		}
	
	}

	return View;
});

