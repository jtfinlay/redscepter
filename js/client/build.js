require(["./client"], function(client) {

	// This function is called when scripts/helper/util.js is loaded.
	// If client.js calls define(), then this function is not fire until
	// client's dependencies have loaded, and the client argument will hold
	// the module value for "./client"
	
	client.init();
	
});